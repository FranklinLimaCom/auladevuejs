var contactUs = {
	data: function() {
		return {
			email: 'info@mycompany.com'
		};
	},
	template: `
		<div>
			<h1>Contact Us</h1>
			<p>Please send an e-mail to: {{ email }}</p>
		</div>
	`
};

new Vue({
	el: '#app1',
	components: {
		'contact-us': contactUs
	}
});

new Vue({
	el: '#app2',
});